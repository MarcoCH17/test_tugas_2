<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url', 'form'));
		$this->load->library(array('form_validation','Recaptcha'));
	
	}
	public function index()
	{

		$data['title']='Buku';
		$this->load->view('templates/index_header',$data);
		$this->load->view('index/index');
		$this->load->view('templates/index_footer');
	}
	public function login()
	{
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('password','Password','trim|required');
		if($this->form_validation->run()==false){
		$data['title']='Login';
		$data = array(
			'captcha' => $this->recaptcha->getWidget(),
            'script_captcha' => $this->recaptcha->getScriptTag(),
		);
		$this->load->view('templates/auth_header',$data);
		$this->load->view('auth/login');
		$this->load->view('templates/auth_footer');
		}
		else{
			$this->_login();
		}
		
	}
	private function _login(){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$recaptcha = $this->input->post('g-recaptcha-response');
			$user =$this->db->get_where('user',['email' => $email])->row_array();
			$response = $this->recaptcha->verifyResponse($recaptcha);
			//user ada
			if($response['success']){
				if($user){
					// user aktif
					if($user['is_active']==1){
						//password
						if(password_verify($password,$user['password'])){
							$data =[
								'email' => $user['email'],
								'role_id'=> $user['role_id']
							];
							$this->session->set_userdata($data);
							redirect('user');
							
						}else{
							$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Password Salah</div>');
					redirect('auth/login');
						}
					}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Email is not activated</div>');
					redirect('auth/login');
					}
				}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Email is not register</div>');
					redirect('auth/login');
	
				}
			}
			else{
				$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Please Press Capthca</div>');
				redirect('auth/login');
			}
	}
	public function registration()
	{
		$this->form_validation->set_rules('name','Name','required|trim');
		$this->form_validation->set_rules('email','Email','required|trim|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('password1','Password','required|trim|min_length[3]|matches[password2]',[
			'matches'=> 'password dont match!','min_length'=> 'Password too short!'
		]);
		$this->form_validation->set_rules('password2','Password','required|trim|matches[password1]');
		if ($this->form_validation->run()==false){
		$data['title']='Register';
		$this->load->view('templates/auth_header',$data);
		$this->load->view('auth/registration');
		$this->load->view('templates/auth_footer');
	}else {
		$data =[
			'name' => htmlspecialchars($this->input->post('name',true)),
			'email' => htmlspecialchars($this->input->post('email',true)),
			'image' => 'default.jpg',
			'password' => password_hash($this->input->post('password1'),PASSWORD_DEFAULT),
			'role_id'=> 2,
			'is_active' => 1,
			'date_created' => time()
		];

		$this->db->insert('user',$data);
		$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">Success</div>');
		redirect('auth/login');
	}
	}
	public function logout()
	{
		$this->session->unset_userdata['email'];
		$this->session->unset_userdata['role_id'];

		$this->session->set_flashdata('message','<div class="alert alert-primary" role="alert">Success Logout</div>');
		redirect('auth/login');
	}
	public function send(){
        $this->load->library('phpmailer_lib');
        
        $mail = $this->phpmailer_lib->load();
        
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'bobohomarco@gmail.com';
        $mail->Password = '1234sarman';
        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;
        
        $mail->setFrom('bobohomarco@gmail.com', 'Marco');
        
      
        $mail->addAddress('marcochristopher17@gmail.com');
        
        // Add cc or bcc 
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');
        
        
        $mail->Subject = 'Send Email via SMTP using PHPMailer in CodeIgniter';
        
        
        $mail->isHTML(true);
        
       
        $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
            <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";
        $mail->Body = $mailContent;
        
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }
}